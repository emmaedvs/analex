package analex;

/**
 *
 * @author Emmanuel David Ventura Silva
 */

public enum Tokens {
    IDENTIFICADOR,
    OR,
    AND,
    NOT,
    NUMERO,
    IGUAL,
    NO_DEFINIDO,
    SEPARADOR,
}
